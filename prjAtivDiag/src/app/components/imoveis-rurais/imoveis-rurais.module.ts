import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImoveisRuraisComponent } from './imoveis-rurais.component';
import { DestaquesComponent } from './destaques/destaques.component';



@NgModule({
  declarations: [
    ImoveisRuraisComponent,
    DestaquesComponent
  ],
  exports: [
    ImoveisRuraisComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ImoveisRuraisModule { }
