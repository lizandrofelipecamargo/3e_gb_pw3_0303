import { HomeComponent } from './home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopoComponent } from './topo/topo.component';
import { DestaquesComponent } from './destaques/destaques.component';
import { FiltroComponent } from './filtro/filtro.component';
import { FeedbackComponent } from './feedback/feedback.component';



@NgModule({
  declarations: [
    TopoComponent,
    DestaquesComponent,
    FiltroComponent,
    FeedbackComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    
  ]
})
export class HomeModule { }
